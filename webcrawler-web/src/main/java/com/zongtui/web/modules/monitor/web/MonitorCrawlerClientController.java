/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.zongtui.web.modules.monitor.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zongtui.web.common.config.Global;
import com.zongtui.web.common.persistence.Page;
import com.zongtui.web.common.web.BaseController;
import com.zongtui.web.common.utils.StringUtils;
import com.zongtui.web.modules.monitor.entity.MonitorCrawlerClient;
import com.zongtui.web.modules.monitor.service.MonitorCrawlerClientService;

/**
 * 客户端监控Controller
 * @author zhangfeng
 * @version 2015-05-02
 */
@Controller
@RequestMapping(value = "${adminPath}/monitor/monitorCrawlerClient")
public class MonitorCrawlerClientController extends BaseController {

	@Autowired
	private MonitorCrawlerClientService monitorCrawlerClientService;
	
	@ModelAttribute
	public MonitorCrawlerClient get(@RequestParam(required=false) String id) {
		MonitorCrawlerClient entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = monitorCrawlerClientService.get(id);
		}
		if (entity == null){
			entity = new MonitorCrawlerClient();
		}
		return entity;
	}
	
	@RequestMapping(value = {"list", ""})
	public String list(MonitorCrawlerClient monitorCrawlerClient, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MonitorCrawlerClient> page = monitorCrawlerClientService.findPage(new Page<MonitorCrawlerClient>(request, response), monitorCrawlerClient); 
		model.addAttribute("page", page);
		return "modules/monitor/monitorCrawlerClientList";
	}


}