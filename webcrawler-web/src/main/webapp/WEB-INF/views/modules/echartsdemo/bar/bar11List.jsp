<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>echarts demo管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
	</script>

</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/echartsdemo/echartsdemo/bar11">echarts demo列表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="echartsdemo" action="${ctx}/echartsdemo/echartsdemo/bar11" method="post" class="breadcrumb form-search">
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	
	<div id="main" style="height:400px"></div>
	<script src="${ctxStatic}/echarts/echarts.js"></script>
<script type="text/javascript">
        // 路径配置
        require.config({
            paths: {
                echarts: '${ctxStatic}/echarts'
            }
        });
        
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/line', // 使用线状图就加载line模块，按需加载
                'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main')); 
                
                var labelRight = {normal: {label : {position: 'right'}}};
                option = {
                    title: {
                        text: '交错正负轴标签',
                        subtext: 'From ExcelHome',
                        sublink: 'http://e.weibo.com/1341556070/AjwF2AgQm'
                    },
                    tooltip : {
                        trigger: 'axis',
                        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                        }
                    },
                    toolbox: {
                        show : true,
                        feature : {
                            mark : {show: true},
                            dataView : {show: true, readOnly: false},
                            magicType : {show: true, type: ['line', 'bar']},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    grid: {
                        y: 80,
                        y2: 30
                    },
                    xAxis : [
                        {
                            type : 'value',
                            position: 'top',
                            splitLine: {lineStyle:{type:'dashed'}},
                        }
                    ],
                    yAxis : [
                        {
                            type : 'category',
                            axisLine: {show: false},
                            axisLabel: {show: false},
                            axisTick: {show: false},
                            splitLine: {show: false},
                            data : ['ten', 'nine', 'eight', 'seven', 'six', 'five', 'four', 'three', 'two', 'one']
                        }
                    ],
                    series : [
                        {
                            name:'生活费',
                            type:'bar',
                            stack: '总量',
                            itemStyle : { normal: {
                                color: 'orange',
                                borderRadius: 5,
                                label : {
                                    show: true,
                                    position: 'left',
                                    formatter: '{b}'
                                }
                            }},
                            data:[
                                {value:-0.07, itemStyle:labelRight},
                                {value:-0.09, itemStyle:labelRight},
                                0.2, 0.44, 
                                {value:-0.23, itemStyle:labelRight},
                                0.08,
                                {value:-0.17, itemStyle:labelRight},
                                0.47,
                                {value:-0.36, itemStyle:labelRight},
                                0.18
                            ]
                        }
                    ]
                };
        
                // 为echarts对象加载数据 
                myChart.setOption(option); 
            }
        );
    </script>
	
</body>
</html>