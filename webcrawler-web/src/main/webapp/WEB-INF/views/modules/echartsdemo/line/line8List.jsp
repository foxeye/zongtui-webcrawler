<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>echarts demo管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>

</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/echartsdemo/echartsdemo/line8">echarts demo列表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="echartsdemo" action="${ctx}/echartsdemo/echartsdemo/line8" method="post" class="breadcrumb form-search">
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	
	<div id="main" style="height:400px"></div>
	<script src="${ctxStatic}/echarts/echarts.js"></script>
<script type="text/javascript">
        // 路径配置
        require.config({
            paths: {
                echarts: '${ctxStatic}/echarts'
            }
        });
        
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/line', // 使用线状图就加载line模块，按需加载
                'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main')); 
                
            var option = {
            	    title : {
            	        text : '时间坐标折线图',
            	        subtext : 'dataZoom支持'
            	    },
            	    tooltip : {
            	        trigger: 'item',
            	        formatter : function (params) {
            	            var date = new Date(params.value[0]);
            	            data = date.getFullYear() + '-'
            	                   + (date.getMonth() + 1) + '-'
            	                   + date.getDate() + ' '
            	                   + date.getHours() + ':'
            	                   + date.getMinutes();
            	            return data + '<br/>'
            	                   + params.value[1] + ', ' 
            	                   + params.value[2];
            	        }
            	    },
            	    toolbox: {
            	        show : true,
            	        feature : {
            	            mark : {show: true},
            	            dataView : {show: true, readOnly: false},
            	            restore : {show: true},
            	            saveAsImage : {show: true}
            	        }
            	    },
            	    dataZoom: {
            	        show: true,
            	        start : 70
            	    },
            	    legend : {
            	        data : ['series1']
            	    },
            	    grid: {
            	        y2: 80
            	    },
            	    xAxis : [
            	        {
            	            type : 'time',
            	            splitNumber:10
            	        }
            	    ],
            	    yAxis : [
            	        {
            	            type : 'value'
            	        }
            	    ],
            	    series : [
            	        {
            	            name: 'series1',
            	            type: 'line',
            	            showAllSymbol: true,
            	            symbolSize: function (value){
            	                return Math.round(value[2]/10) + 2;
            	            },
            	            data: (function () {
            	                var d = [];
            	                var len = 0;
            	                var now = new Date();
            	                var value;
            	                while (len++ < 200) {
            	                    d.push([
            	                        new Date(2014, 9, 1, 0, len * 10000),
            	                        (Math.random()*30).toFixed(2) - 0,
            	                        (Math.random()*100).toFixed(2) - 0
            	                    ]);
            	                }
            	                return d;
            	            })()
            	        }
            	    ]
            	};
        
                // 为echarts对象加载数据 
                myChart.setOption(option); 
            }
        );
    </script>
	
</body>
</html>